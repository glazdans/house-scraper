package gl.housescraper

import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import java.util.*
import javax.mail.util.ByteArrayDataSource


@Service
class MessagingService(val emailSender: JavaMailSender) {

    fun sendMessage(adverts: Map<SSUrl, List<Advert>>) {
        val advertCount = adverts.values.map { it.count() }.sum()
        val bodies = adverts.map { generateBody(it.value, it.key.name) }
        val message = emailSender.createMimeMessage()
        val helper = MimeMessageHelper(message, true)
        helper.setFrom("noreply@baeldung.com")
        helper.setTo("g.lazdans@gmail.com")
        helper.setSubject("${advertCount} new house adverts!")
        helper.setText(htmlTemplate(bodies.joinToString("\n")), true)

        adverts.values.forEach {
            it.forEach{ ad ->
                val dataSource = getImageDataSource(ad.imageBase64)
                helper.addInline(ad.id.toString(), dataSource)
            }
        }
        emailSender.send(message)
    }

    private fun generateBody(ads: List<Advert>, region: String): String {
        val tableRows = ads.map { generateAdvertRow(it) }
        return """
            <table>
                <tr>
                    <th>$region</th>
                    <th>Sludinājums</th>
                    <th>Pagasts</th>
                    <th>m2</th>
                    <th>Stāvi</th>
                    <th>Zem. pl.</th>
                    <th>Cena</th>
                </tr>
                ${tableRows.joinToString("\n")}
            </table>
        """.trimMargin()
    }

    private fun getImageDataSource(base64Image: String): ByteArrayDataSource {
        val rawImage: ByteArray = Base64.getDecoder().decode(base64Image)
        return ByteArrayDataSource(rawImage, "image/png")
    }

    private fun generateAdvertRow(ad: Advert): String {
        return """
            <tr>
                <th><img src="cid:${ad.id}" alt="img" /></th>
                <th><a href='https://www.ss.lv${ad.advertUrl}'>${ad.headline}</a></th>
                <th>${ad.location}</th>
                <th>${ad.size}</th>
                <th>${ad.stories}</th>
                <th>${ad.landSize}</th>
                <th>${ad.price}</th>
            </tr>
        """.trimIndent()
    }

    private fun htmlTemplate(body: String): String {
        return """
            <!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd”>
            <html xmlns=“https://www.w3.org/1999/xhtml”>
                <head>
                    <title>Test Email Sample</title>
                    <meta http–equiv=“Content-Type” content=“text/html; charset=UTF-8” />
                    <meta http–equiv=“X-UA-Compatible” content=“IE=edge” />
                    <meta name=“viewport” content=“width=device-width, initial-scale=1.0 “ />
                    <style>
                    </style>
                </head>
                <body>
                $body
                </body>
            </html>
        """.trimIndent()
    }

}