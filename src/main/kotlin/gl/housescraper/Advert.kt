package gl.housescraper

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Lob

@Entity
class Advert(var headline: String,
             var location: String,
             var size: String,
             var stories: String,
             var landSize: String,
             var price: String,
             var advertUrl: String,
             var listUrl: String,
             @Lob var imageBase64: String,
             @Id @GeneratedValue var id: Long? = null)
