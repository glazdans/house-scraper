package gl.housescraper

import org.springframework.data.repository.CrudRepository

interface AdvertRepository : CrudRepository<Advert, Long> {
    fun findByListUrl(listUrl: String): Iterable<Advert>
}