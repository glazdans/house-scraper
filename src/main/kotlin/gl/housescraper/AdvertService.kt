package gl.housescraper

import org.springframework.stereotype.Service

@Service
class AdvertService(val advertRepository: AdvertRepository) {

    // Query for existing urls in DB will increase performance in worst case scenarios
    fun addNewAdverts(url: String, adverts: List<Advert>): List<Advert> {
        val existingAdverts = advertRepository.findByListUrl(url)
        val newAdverts = adverts.filter { advert ->
            existingAdverts.find { it.advertUrl == advert.advertUrl } == null
        }
        advertRepository.saveAll(newAdverts)
        return newAdverts
    }

    fun filterAdvertsByCriteria(adverts: List<Advert>): List<Advert> {
        return adverts.filter {
            val priceString = it.price.split(" ").get(0).replace(",", "")
            priceString.toIntOrNull() != null
        }.filter {
            val priceSplit = it.price.split(" ")
            !(priceSplit.contains("€/mēn.") || priceSplit.contains("€/dienā"))
        }.filter {
            val priceString = it.price.split(" ").get(0).replace(",", "")
            priceString.toInt() <= Filters.maxPrice
        }
    }
}

object Filters {
    val maxPrice = 85000
}