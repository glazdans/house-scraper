package gl.housescraper

import org.apache.commons.io.IOUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.springframework.stereotype.Service
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

@Service
class Scraper {

    fun getAdverts(url: String): List<Advert> {
        val advertList = ArrayList<Advert>(30);
        try {
            val document: Document = Jsoup.connect(url).get()
            val form = document.getElementsByTag("form");
            val adTable = form.get(0).child(2)

            adTable.child(0).children().filter {
                it.id() != "head_line"
            }.forEach {
                val children = it.children()
                try {
                    val imageUrl = children[1].child(0).child(0).absUrl("src")
                    val advertUrl = children[1].child(0).attr("href")
                    val headline = children[2].child(0).child(0).text()
                    val location = children[3].text()
                    val size = children[4].text()
                    val stories = children[5].text()
                    val landSize = children[6].text()
                    val price = children[7].text()
                    val advert = Advert(headline, location, size, stories, landSize, price, advertUrl, url, getImageAsBase64(imageUrl))
                    advertList.add(advert)
                } catch (e: Exception) {
                    println(e.stackTraceToString())
                    println(children.html())
                }
            }
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return advertList;
    }

    private fun getImageAsBase64(url: String): String {
        val bytes = IOUtils.toByteArray(URL(url))
        return Base64.getEncoder().encodeToString(bytes)
    }
}
