package gl.housescraper

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class Scheduler(val scraper: Scraper, val advertService: AdvertService, val messagingService: MessagingService) {
    var logger: Logger = LoggerFactory.getLogger(Scheduler::class.java)
    @Scheduled(fixedDelay = 15 * 60 * 1000)
    fun doJob(){
    val advertMap = HashMap<SSUrl, List<Advert>>()
        SSUrl.values().forEach {
            val adverts = scraper.getAdverts(it.url)
            val newAdverts = advertService.addNewAdverts(it.url, adverts)
            val filteredAdverts = advertService.filterAdvertsByCriteria(newAdverts)
            if(filteredAdverts.isNotEmpty()) {
                advertMap[it] = filteredAdverts
            }
            Thread.sleep((1000L..3000L).random())
        }

        if(advertMap.isNotEmpty()) {
            messagingService.sendMessage(advertMap)
        }
        logger.info("Finished refreshing adverts")
    }

}

enum class SSUrl(val url: String) {
    RIGA("https://www.ss.lv/lv/real-estate/homes-summer-residences/riga/all/"),
    RIGA_REGION("https://www.ss.lv/lv/real-estate/homes-summer-residences/riga-region/all/"),
    JELGAVA("https://www.ss.lv/lv/real-estate/homes-summer-residences/jelgava-and-reg/"),
    OGRE("https://www.ss.lv/lv/real-estate/homes-summer-residences/ogre-and-reg/"),
    BAUSKA("https://www.ss.lv/lv/real-estate/homes-summer-residences/bauska-and-reg/"),
    CESIS("https://www.ss.lv/lv/real-estate/homes-summer-residences/cesis-and-reg/")

}
