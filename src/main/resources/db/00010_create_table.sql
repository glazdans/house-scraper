--liquibase formatted sql
--changeset gla:add_advert_table
CREATE SEQUENCE HIBERNATE_SEQUENCE START WITH 1 INCREMENT BY 1;

create table advert (
                        id bigint not null,
                        advert_url varchar(255),
                        headline varchar(255),
                        land_size varchar(255),
                        list_url varchar(255),
                        location varchar(255),
                        price varchar(255),
                        size varchar(255),
                        stories varchar(255),
                        image_base64 clob,
                        primary key (id)
);

--rollback DROP TABLE advert
--rollback DROP SEQUENCE HIBERNATE_SEQUENCE